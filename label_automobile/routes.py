def includeme(config):
    config.add_static_view('static', 'static', cache_max_age=3600)

    # Get user list
    config.add_route('user.list', '/user', request_method="GET")
    # config.add_route('user.find_by_email', '/user/email', request_method="GET")

    # Product list
    config.add_route('product.list', '/product', request_method="GET")

    # Get single product by uuid
    config.add_route('product.get', '/product/{uuid}', request_method="GET")

    # Add product to cart
    config.add_route('cart.add', '/cart', request_method="POST")

    # Delete product from cart
    config.add_route('cart.delete', '/cart/{cart_uuid}/product/{product_uuid}', request_method="DELETE")

    # Order cart contents
    config.add_route('order.cart', '/cart/{cart_uuid}/order', request_method="POST")
