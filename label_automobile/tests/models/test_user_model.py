import unittest
import transaction
from uuid import uuid4

from pyramid import testing


def dummy_request(dbsession):
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('label_automobile.models')
        settings = self.config.get_settings()

        from label_automobile.models import (
            get_engine,
            get_session_factory,
            get_tm_session,
            )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        from label_automobile.models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        from label_automobile.models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)

    def makeUser(self, name, surname, email, active):
        from label_automobile.models import User
        return User(uuid=uuid4(), name=name, surname=surname, email=email, active=active)


class TestCreateUser(BaseTest):

    def test_user_create(self):
        user = self.makeUser(name='test',surname='test', email='test@test.nl', active=1)
        self.assertEqual(user.name, 'test')
        self.assertEqual(user.surname, 'test')
        self.assertEqual(user.email, 'test@test.nl')
        self.assertEqual(user.active, 1)

    def test_email_validation(self):
        with self.assertRaises(Exception):
            self.makeUser(name='test', surname='test', email='testtest.nl', active=1)
