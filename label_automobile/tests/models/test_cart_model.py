import unittest
import transaction

from pyramid import testing


def dummy_request(dbsession):
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('label_automobile.models')
        settings = self.config.get_settings()

        from label_automobile.models import (
            get_engine,
            get_session_factory,
            get_tm_session,
            )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        from label_automobile.models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        from label_automobile.models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)

    def makeProduct(self, name, ean, active):
        from label_automobile.models import Product
        return Product(name=name, ean=ean, active=active)

    def makeUser(self, name, surname, email, active):
        from label_automobile.models import User
        return User(name=name, surname=surname, email=email, active=active)

    def makeCart(self, user, products):
        from label_automobile.models import Cart
        cart = Cart(user=user)
        for product in products:
            cart.products.append(product)

        return cart


class TestCreateCart(BaseTest):

    def test_cart_create_cart_single_product_add(self):
        product = self.makeProduct(name='test', ean='eans', active=1)
        user = self.makeUser(name='test', surname='test', email='test@test.nl', active=1)

        cart = self.makeCart(user=user, products=[product])

        # Check if user is set correctly
        self.assertEqual(cart.user, user)

        # Check if product is set correctly
        self.assertEqual(cart.products[0], product)

    def test_cart_create_cart_multiple_products_add(self):
        product1 = self.makeProduct(name='product1', ean='productean1', active=1)
        product2 = self.makeProduct(name='product2', ean='productean2', active=1)
        user = self.makeUser(name='test', surname='test', email='test@test.nl', active=1)

        cart = self.makeCart(user=user, products=[product1, product2])

        # Check if user is set correctly
        self.assertEqual(cart.user, user)

        # Check if products are set correctly
        self.assertEqual(cart.products[0], product1)
        self.assertEqual(cart.products[1], product2)

    def test_cart_delete_product(self):
        product1 = self.makeProduct(name='product1', ean='productean1', active=1)
        product2 = self.makeProduct(name='product2', ean='productean2', active=1)
        user = self.makeUser(name='test', surname='test', email='test@test.nl', active=1)

        cart = self.makeCart(user=user, products=[product1, product2])

        cart.products.remove(product1)

        # Check if only 1 product remains in cart
        self.assertEqual(len(cart.products), 1)

    def test_cart_delete_all_products(self):
        product1 = self.makeProduct(name='product1', ean='productean1', active=1)
        product2 = self.makeProduct(name='product2', ean='productean2', active=1)
        user = self.makeUser(name='test', surname='test', email='test@test.nl', active=1)

        cart = self.makeCart(user=user, products=[product1, product2])

        for product in list(cart.products):
            cart.products.remove(product)

        # Check if no products remain in cart
        self.assertEqual(len(cart.products), 0)


