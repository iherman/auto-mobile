import unittest
import transaction

from pyramid import testing


def dummy_request(dbsession):
    return testing.DummyRequest(dbsession=dbsession)


class BaseTest(unittest.TestCase):
    def setUp(self):
        self.config = testing.setUp(settings={
            'sqlalchemy.url': 'sqlite:///:memory:'
        })
        self.config.include('label_automobile.models')
        settings = self.config.get_settings()

        from label_automobile.models import (
            get_engine,
            get_session_factory,
            get_tm_session,
        )

        self.engine = get_engine(settings)
        session_factory = get_session_factory(self.engine)

        self.session = get_tm_session(session_factory, transaction.manager)

    def init_database(self):
        from label_automobile.models.meta import Base
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        from label_automobile.models.meta import Base

        testing.tearDown()
        transaction.abort()
        Base.metadata.drop_all(self.engine)

    def makeOrder(self, user, products):
        from label_automobile.models import Order

        order = Order(user=user)
        for product in products:
            order.products.append(product)

        return order

    def makeUser(self, name, surname, email, active):
        from label_automobile.models import User
        return User(name=name, surname=surname, email=email, active=active)

    def makeProduct(self, name, ean, active):
        from label_automobile.models import Product
        return Product(name=name, ean=ean, active=active)


class TestCreateOrder(BaseTest):

    def test_create_order_single_product_add(self):
        self.assertEqual(1, 1)
        product = self.makeProduct(name='test', ean='eans', active=1)
        user = self.makeUser(name='test', surname='test', email='test@test.nl', active=1)

        order = self.makeOrder(user=user, products=[product])

        # Check if user is set correctly
        self.assertEqual(order.user, user)

        # Check if product is set correctly
        self.assertEqual(order.products[0], product)
