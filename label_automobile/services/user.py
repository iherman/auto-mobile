from label_automobile.repositories.user import UserRepository


class UserService:
    def __init__(self, session):
        self._repository = UserRepository(session)

    def find_by_email(self, email):
        """
        Find a user by email address
        :param email: a user's email address
        :return: User
        """
        return self._repository.find_by_email(email)

    def index(self):
        """
        Find all users
        :return: Users
        """
        users = self._repository.index()

        return self.format_json_response_user_list(users)

    def format_json_response_user_list(self, users):
        """
        Format result to json
        :param: Users
        :return: json
        """
        json_response = {
            'users': []
        }

        for user in users:
            json_response['users'].append(
                {
                    'uuid': str(user.uuid),
                    'name': user.name,
                    'surname': user.surname,
                }
            )

        return json_response
