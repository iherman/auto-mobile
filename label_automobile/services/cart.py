from label_automobile.models.cart import Cart
from label_automobile.models.cart_products import CartProducts
from label_automobile.repositories.cart import CartRepository
from label_automobile.repositories.product import ProductRepository
from label_automobile.repositories.user import UserRepository


class CartService:
    def __init__(self, session):
        self._session = session
        self._repository = CartRepository(session)
        self._product_repository = ProductRepository(session)
        self._user_repository = UserRepository(session)

    def get(self, uuid):
        """
        Get cart by cart UUID
        :param uuid: uuid of Cart
        :return: Cart | None
        """
        cart = self._repository.find_by_uuid(uuid)
        return cart

    def find_by_user_uuid(self, user_uuid):
        """
        Get cart by user UUID
        :param user_uuid: uuid of User
        :return: Cart | None
        """
        # Check if user exists
        user = self._user_repository.get(user_uuid)
        if user is None:
            raise Exception('No user found with uuid ' + user_uuid)

        cart = self._repository.find_by_user(user)
        return cart

    def add_product(self, product_uuid, user_uuid):
        """
        Add product to cart
        :param product_uuid: uuid of an existing product
        :param user_uuid: uuid of an existing user
        :return: Cart
        """
        # Check if product exists
        product = self._product_repository.get(product_uuid)
        if product is None:
            raise Exception('No product found with uuid ' + product_uuid)

        # Check if user exists
        user = self._user_repository.get(user_uuid)
        if user is None:
            raise Exception('No user found with uuid ' + user_uuid)

        # Get cart by user
        cart = self._repository.find_by_user(user)

        # If cart does not exist create a new one
        if cart is None:
            cart = Cart(user=user)

        for cart_product in cart.products:
            if cart_product.product_id == product.id:
                print("Found the same product in cart adding 1 to quantity")
                cart_product.quantity += 1
                quantity = cart_product.quantity
                break
        else:
            quantity = None

        if quantity is None:
            cart_product = CartProducts(quantity=1)
            cart_product.product = product
            cart.products.append(cart_product)

        self._session.add(cart)
        self._session.flush()

        return self.format_json_response(cart)

    def remove_product(self, product_uuid, cart_uuid):
        """
        Add product to cart
        :param product_uuid: uuid of an existing product
        :param user_uuid: uuid of an existing user
        :return: Cart
        """
        # Get cart by user
        cart = self._repository.find_by_uuid(cart_uuid)

        if cart is None:
            return None

        # Check if product exists
        product = self._product_repository.get(product_uuid)
        if product is None:
            raise Exception('No product found with uuid ' + product_uuid)

        for cart_product in cart.products:
            if cart_product.product_id == product.id:
                print("Found the same product in cart removing 1 to quantity")
                cart_product.quantity -= 1
                break

        self._session.add(cart)

        return self.format_json_response(cart)

    def format_json_response(self, cart):
        """
        Format result to json
        :param: Cart
        :return: json
        """
        json_response = {
            'cart': str(cart.uuid),
            'products': []
        }

        for cart_product in cart.products:
            product = self._product_repository.get_by_id(cart_product.product_id)
            json_response['products'].append(
                {
                    'quantity': cart_product.quantity,
                    'uuid': str(product.uuid),
                    'name': str(product.name)
                }
            )

        return json_response

