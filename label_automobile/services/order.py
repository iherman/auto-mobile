from label_automobile.models.order import Order
from label_automobile.repositories.order import OrderRepository
from label_automobile.repositories.cart import CartRepository
from label_automobile.repositories.product import ProductRepository
from datetime import datetime


class OrderService:
    def __init__(self, session):
        self._session = session
        self._repository = OrderRepository(session)
        self._cart_repository = CartRepository(session)
        self._product_repository = ProductRepository(session)

    def create_order_from_cart(self, cart_uuid, delivery_date, delivery_time):
        """
        Create an order from cart
        :param cart_uuid: uuid of Cart
        :param delivery_date: preferred date of delivery
        :param delivery_time: preferred time of delivery
        :return: Order
        """
        cart = self._cart_repository.find_by_uuid(cart_uuid)
        user = cart.user

        # format date
        datetime_object = datetime.strptime(delivery_date + " " + delivery_time, '%Y-%m-%d %H:%M')
        # format datetime

        order = Order(user=user, delivery_time=datetime_object)
        self._session.add(order)

        for cart_product in cart.products:
            product = self._product_repository.get_by_id(cart_product.product_id)
            self._repository.add_product(order, product)

        return self.format_json_response(order)

    def format_json_response(self, order):
        """
        Format result to json
        :param: Order
        :return: json
        """
        json_response = {
            'order': str(order.uuid),
            'user': str(order.user.uuid),
            'delivery_date': order.delivery_time.date().strftime('%Y-%m-%d'),
            'delivery_time': order.delivery_time.time().strftime('%H:%M'),
            'products': []
        }

        for order_products in order.products:
            json_response['products'].append(
                {
                    'uuid': str(order_products.uuid),
                    'name': order_products.name,
                    'ean': order_products.ean
                }
            )

        return json_response

