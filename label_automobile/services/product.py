from label_automobile.repositories.product import ProductRepository


class ProductService:
    def __init__(self, session):
        self._repository = ProductRepository(session)

    def index(self):
        """
        Find all products
        :return: Products
        """
        products = self._repository.index()

        return self.format_json_response_product_list(products)

    def get(self, uuid):
        """
        Get single product by uuid
        :param uuid: Product UUID
        :return: Product
        """
        product = self._repository.get(uuid)

        return self.format_json_response_product_get(product)

    def add(self, product):
        """
        Add product
        :param product: Product a user's email address
        :return: User
        """
        self._repository.add_product(product)

    def format_json_response_product_get(self, product):
        """
        Format result to json
        :param: Product
        :return: json
        """
        json_response = [
            {
                'uuid': str(product.uuid),
                'name': product.name,
                'ean': product.ean
            }
        ]

        return json_response

    def format_json_response_product_list(self, products):
        """
        Format result to json
        :param: Products
        :return: json
        """
        json_response = {
            'products': []
        }

        for product in products:
            json_response['products'].append(
                {
                    'uuid': str(product.uuid),
                    'name': product.name,
                }
            )

        return json_response

