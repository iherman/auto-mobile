from sqlalchemy import Column, Integer, ForeignKey, Date, DateTime
from sqlalchemy.orm import relationship
from sqlalchemy.schema import Table

from label_automobile.models.meta import Base, BaseModel

association_table_order_products = Table('order_products', Base.metadata,
    Column('order.id', Integer, ForeignKey('order.id')),
    Column('product.id', Integer, ForeignKey('product.id'))
)


class Order(BaseModel, Base):
    __tablename__ = 'order'

    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User")

    products = relationship("Product", secondary=association_table_order_products)

    delivery_time = Column(DateTime, nullable=True)
