from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship

from label_automobile.models.meta import Base, BaseModel


class CartProducts(BaseModel, Base):
    __tablename__ = 'cart_products'
    cart_id = Column(Integer, ForeignKey('cart.id'), primary_key=True)
    product_id = Column(Integer, ForeignKey('product.id'), primary_key=True)
    quantity = Column(Integer)

    product = relationship("Product")
