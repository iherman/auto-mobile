from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship
from label_automobile.models.meta import Base, BaseModel


class Cart(BaseModel, Base):
    __tablename__ = 'cart'
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship("User")

    products = relationship("CartProducts")

