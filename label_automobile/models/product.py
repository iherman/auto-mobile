from sqlalchemy import Column, String, Boolean, Integer

from label_automobile.models.meta import Base, BaseModel


class Product(BaseModel, Base):
    __tablename__ = 'product'

    name = Column(String, nullable=False)
    ean = Column(String, nullable=False)

    active = Column(Boolean, nullable=False, default=False)
