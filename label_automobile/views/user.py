from pyramid.view import view_defaults, view_config
from label_automobile.services.user import UserService


@view_defaults(renderer='json')
class UserView:
    def __init__(self, request):
        self.request = request
        self.session = request.dbsession

    @view_config(route_name='user.list')
    def list(self):
        service = UserService(self.session)
        return service.index()
