from pyramid.view import view_defaults, view_config

from label_automobile.services.cart import CartService


@view_defaults(renderer='json')
class CartView:
    def __init__(self, request):
        self.request = request
        self.session = request.dbsession

    @view_config(route_name='cart.add')
    def cart_add(self):
        json_request = self.request.json

        product_uuid = json_request['product_uuid']
        user_uuid = json_request['user_uuid']

        cart_service = CartService(self.session)

        json_response = cart_service.add_product(product_uuid, user_uuid)

        self.request.response.status_code = 201
        self.request.response.content_type = "application.json"
        self.request.response.json = json_response

        return self.request.response

    @view_config(route_name='cart.delete')
    def cart_delete(self):
        product_uuid = self.request.matchdict['product_uuid']
        cart_uuid = self.request.matchdict['cart_uuid']

        cart_service = CartService(self.session)

        json_response = cart_service.remove_product(product_uuid, cart_uuid)

        self.request.response.status_code = 200
        self.request.response.content_type = "application.json"

        self.request.response.json = json_response

        return self.request.response
