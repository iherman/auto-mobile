from pyramid.view import view_defaults, view_config

from label_automobile.services.order import OrderService


@view_defaults(renderer='json')
class OrderView:
    def __init__(self, request):
        self.request = request
        self.session = request.dbsession

    @view_config(route_name='order.cart')
    def order_from_cart(self):
        cart_uuid = self.request.matchdict['cart_uuid']

        json_request = self.request.json
        order_date = json_request['order_date']
        order_time = json_request['order_time']

        order_service = OrderService(self.session)

        json_response = order_service.create_order_from_cart(cart_uuid, order_date, order_time)

        self.request.response.status_code = 201
        self.request.response.content_type = "application.json"
        self.request.response.json = json_response

        return self.request.response
