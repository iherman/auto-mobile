from pyramid.view import view_defaults, view_config

from label_automobile.services.product import ProductService


@view_defaults(renderer='json')
class ProductView:
    def __init__(self, request):
        self.request = request
        self.session = request.dbsession

    @view_config(route_name='product.list')
    def list(self):
        service = ProductService(self.session)

        json_response = service.index()
        self.request.response.status_code = 200
        self.request.response.content_type = "application.json"

        self.request.response.json = json_response
        return service.index()

    @view_config(route_name='product.get')
    def get(self):
        uuid = self.request.matchdict['uuid']
        service = ProductService(self.session)

        json_response = service.get(uuid)
        self.request.response.status_code = 200
        self.request.response.content_type = "application.json"

        self.request.response.json = json_response

        return self.request.response
