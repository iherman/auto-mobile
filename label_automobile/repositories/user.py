from label_automobile.models.user import User


class UserRepository:
    def __init__(self, session):
        self.session = session
        self.model = User

    def index(self):
        return self.session.query(User)

    def get(self, uuid):
        user = self.session.query(User).filter(User.uuid == uuid).one_or_none()
        print(user)
        print(uuid)
        return user

    def find_by_email(self, email):
        return self.session.query(User).filter(
            User.email == email).one()
