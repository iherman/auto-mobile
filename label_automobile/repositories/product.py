from label_automobile.models.product import Product


class ProductRepository:
    def __init__(self, session):
        self.session = session
        self.model = Product

    def index(self):
        return self.session.query(Product)

    def get(self, uuid):
        product = self.session.query(Product).filter(
            Product.uuid == uuid).one_or_none()

        return product

    def get_by_id(self, id):
        product = self.session.query(Product).filter(
            Product.id == id).one_or_none()

        return product
