from label_automobile.models.order import Order


class OrderRepository:
    def __init__(self, session):
        self.session = session
        self.model = Order

    def add_product(self, order, product):
        order.products.append(product)
        self.session.add(product)

    def add_deliver_vars(self, delivery_date, delivery_time):
        self.model.delivery_date = delivery_date
        self.model.delivery_time = delivery_time