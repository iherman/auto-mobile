from label_automobile.models.cart import Cart


class CartRepository:
    def __init__(self, session):
        self.session = session
        self.model = Cart

    def find_by_user(self, user):
        cart = self.session.query(Cart).filter(
            Cart.user == user).one_or_none()

        return cart

    def find_by_uuid(self, uuid):
        cart = self.session.query(Cart).filter(
            Cart.uuid == uuid).one()

        return cart

    def add_product(self, product):
        self.session.add(product)

    def delete_product(self, product):
        self.session.remove(product)
