Label Automobile
================

Getting Started
---------------

- Change directory into your newly created project.

    cd label_automobile

- Create a Python virtual environment.

    python3 -m venv env

- Activate virtual environment
    source env/bin/activate

- Upgrade packaging tools.

    pip install --upgrade pip setuptools

- Install the project in editable mode with its testing requirements.

    pip install -e ".[testing]"

- Change your settings
    development.ini -> Database settings and possibly application port

- Configure the database.

    init_mob_db development.ini

- Add test data to the database

    mock_mob_db development.ini

- Run your project's tests.

    pytest

- Run your project.

    pserve development.ini


Api routes examples
-----------
*   Get list of users

    ```
    GET /user
    ```
    
* Get list of products

    ```
    GET /product
    ```
    
* Get product details
    
   ```
   GET /product/{product_uuid}
    ```

*   Add product to cart

    ```
    POST /cart
    ```
    
    Post variables: 
    ```
    {
      "product_uuid" : "{product_uuid}",
	      "user_uuid": "{user_uuid}"
    }
    ```
     
        
*   Delete product from cart

    ```
    DELETE /cart/{cart_uuid}/product/{product_uuid}
    ```
    
    
*   Create order from cart
    ```
    POST /cart/{cart_uuid}/order
    ```
    
    Post variables: 
    ```
    {
     "order_date" : "{date}",
	    "order_time": "{time}"
    }
    ```

    date example: 
    
    ```
    "2108-12-24"
    ```
    
	time example: 
	
	```
	"10:24"
	```